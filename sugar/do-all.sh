#!/bin/bash

export CODENAME=$1
export TRIS_VER=1

[ -z $CODENAME ] && echo "usage: $0 etiona|nabia|aramo" && exit

[ $CODENAME = "etiona" ] && export REVISION=9.0
[ $CODENAME = "nabia" ] && export REVISION=10.0
[ $CODENAME = "aramo" ] && export REVISION=11.0

sh update_bundles.sh

rm tmp -rf
rm packages -rf

ls bundles/|grep '.xo'|xargs -i sh package-activity.sh {}

mkdir packages
mv tmp/* packages/
