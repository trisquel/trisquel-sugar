# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Arabian translation for Oficina package.
# Copyright (C) 2007 NATE LSI-USP
# This file is distributed under the same license as the Oficina package.
# 
# Khaled Hosny <khaledhosny@eglug.org>, 2007, 2010.
msgid ""
msgstr ""
"Project-Id-Version: drawing.master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-24 17:39+1100\n"
"PO-Revision-Date: 2017-02-17 07:47+0000\n"
"Last-Translator: khaled <khaledhosny@eglug.org>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Pootle 2.5.1.1\n"
"X-POOTLE-MTIME: 1487317673.000000\n"
"Nplurals=6; Plural=N==0 ? 0: n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 "
": n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: activity/activity.info:2
msgid "Paint"
msgstr "الرسام"

#: activity/activity.info:3
msgid ""
"Picasso? Van Gogh? You can be any of them! Just let the artist inside you "
"free. Transform images in your mind to beautiful paintings."
msgstr ""
"بيكاسو؟ فان جوخ؟ يمكنك أن تصبح أيًا منهم! فقط حرر روح الرسام بداخلك و ابدأ "
"بتحويل كل ما تتخيله إلى أروع الرسوم."

#: widgets.py:17
msgid "Size: "
msgstr "الحجم: "

#: widgets.py:17
msgid "Opacity: "
msgstr "الإعتام: "

#: widgets.py:17
msgid "Circle"
msgstr "دائرة"

#: widgets.py:17
msgid "Square"
msgstr "مربع"

#: widgets.py:31
msgid "Choose brush properties"
msgstr "اختر خصائص الفرشاة"

#: widgets.py:234
msgid "Size"
msgstr "الحجم"

#: widgets.py:248
msgid "Opacity"
msgstr "الإعتام"

#: widgets.py:273
msgid "Shape"
msgstr "شكل"

#: widgets.py:279 toolbox.py:535
msgid "Keep aspect"
msgstr "حافظ على النسبة"

#: widgets.py:304
msgid "Brush properties"
msgstr "خصائص الفرشاة"

#: widgets.py:310
msgid "Stamp properties"
msgstr "خصائص الختم"

#: widgets.py:314
msgid "Eraser properties"
msgstr "خصائص الممحاة"

#: widgets.py:317
msgid "Bucket properties"
msgstr "خصائص الدلو"

#: widgets.py:319
msgid "Picker properties"
msgstr "خصائص المنتقي"

#: widgets.py:321 toolbox.py:410
msgid "Select Area"
msgstr "اختر المنطقة المُرادة"

#: toolbox.py:141
msgid "Edit"
msgstr "حرر"

#: toolbox.py:145
msgid "Shapes properties"
msgstr "خصائص الأشكال"

#: toolbox.py:161
msgid "Shapes"
msgstr "الأشكال"

#: toolbox.py:177
msgid "Fonts"
msgstr "الخطوط"

#: toolbox.py:183
msgid "Image"
msgstr "الصور"

#: toolbox.py:251
msgid "Undo"
msgstr "تراجع"

#: toolbox.py:252
msgid "Redo"
msgstr "أعِد"

#: toolbox.py:253
msgid "Copy"
msgstr "انسخ"

#: toolbox.py:254
msgid "Paste"
msgstr "ألصق"

#: toolbox.py:262
msgid "Clear"
msgstr "امسح"

#: toolbox.py:266 toolbox.py:327
msgid "Enable sound"
msgstr "فعّل الصوت"

#: toolbox.py:325
msgid "Disable sound"
msgstr "عطّل الصوت"

#: toolbox.py:377 toolbox.py:381
msgid "Brush"
msgstr "الفرشاة"

#: toolbox.py:384
msgid "Eraser"
msgstr "الممحاة"

#: toolbox.py:387
msgid "Bucket"
msgstr "الدلو"

#: toolbox.py:390
msgid "Picker"
msgstr "المنتقي"

#: toolbox.py:393
msgid "Stamp"
msgstr "الختم"

#: toolbox.py:400
msgid "Load stamp"
msgstr "حمّل الختم"

#: toolbox.py:529
msgid "Fill"
msgstr "املأ"

#: toolbox.py:552
msgid "Sides: "
msgstr "الجوانب : "

#: toolbox.py:621
msgid "Ellipse"
msgstr "بيضاوي"

#: toolbox.py:624
msgid "Rectangle"
msgstr "مستطيل"

#: toolbox.py:627
msgid "Line"
msgstr "خط مستقيم"

#: toolbox.py:630
msgid "Free form"
msgstr "شكل حر"

#: toolbox.py:633
msgid "Polygon"
msgstr "مضلع"

#: toolbox.py:636
msgid "Heart"
msgstr "قلب"

#: toolbox.py:639
msgid "Parallelogram"
msgstr "متوازي أضلاع"

#: toolbox.py:642
msgid "Arrow"
msgstr "سهم"

#: toolbox.py:645
msgid "Star"
msgstr "نجمة"

#: toolbox.py:648
msgid "Trapezoid"
msgstr "معين"

#: toolbox.py:651
msgid "Triangle"
msgstr "مثلّث"

#: toolbox.py:685
msgid "Type"
msgstr "النوع"

#: toolbox.py:777
msgid "Insert Image"
msgstr "أدرج صورة"

#: toolbox.py:789
msgid "Rotate Left"
msgstr "أدر يسارًا"

#: toolbox.py:793
msgid "Rotate Right"
msgstr "أدر يمينًا"

#: toolbox.py:798
msgid "Horizontal Mirror"
msgstr "مرآة أفقية"

#: toolbox.py:803
msgid "Vertical Mirror"
msgstr "مرآة رأسية"

#: toolbox.py:811
msgid "Grayscale"
msgstr "رمادي"

#: toolbox.py:816
msgid "Rainbow"
msgstr "ألوان الطيف"

#: toolbox.py:821
msgid "Kaleidoscope"
msgstr "مشكال"

#: toolbox.py:825
msgid "Invert Colors"
msgstr "اعكس الألوان"

#: dialogs.py:100
msgid "Done"
msgstr "تم"

#: dialogs.py:128
msgid "Select stamp"
msgstr "اختر ختمًا"

#: fontcombobox.py:90
msgid "Select font"
msgstr "اختر الخط"

#~ msgid "Height"
#~ msgstr "الارتفاع"

#~ msgid "Width"
#~ msgstr "العرض"

#~ msgid "Choose image"
#~ msgstr "اختر صورة"

#~ msgid "Tools"
#~ msgstr "أدوات"

#~ msgid "Text"
#~ msgstr "نص"

#~ msgid "Effects"
#~ msgstr "مؤثِّرات"

#~ msgid "Stroke Color"
#~ msgstr "لون الخط:"

#~ msgid "Pencil"
#~ msgstr "قلم"

#~ msgid "Fill Color"
#~ msgstr "لون الملء"

#~ msgid "Points: "
#~ msgstr "النقاط: "

#~ msgid "1000"
#~ msgstr "1000"

#~ msgid "500"
#~ msgstr "500"

#~ msgid "200"
#~ msgstr "200"

#~ msgid "150"
#~ msgstr "150"

#~ msgid "100"
#~ msgstr "100"

#~ msgid "50"
#~ msgstr "50"

#~ msgid "25"
#~ msgstr "25"

#~ msgid "10"
#~ msgstr "10"

#~ msgid "ZOOM +"
#~ msgstr "قرّب +"

#~ msgid "ZOOM -"
#~ msgstr "بعّد -"

#~ msgid "Tool Color"
#~ msgstr "لون الأداة"

#~ msgid "Rectangular Marquee"
#~ msgstr "معلِّمة مربّعة"

#~ msgid "Fill Color: "
#~ msgstr "لون الملء: "

#~ msgid "1"
#~ msgstr "١"

#~ msgid "2"
#~ msgstr "٢"

#~ msgid "3"
#~ msgstr "٣"

#~ msgid "5"
#~ msgstr "٥"

#~ msgid "20"
#~ msgstr "٢٠"

#~ msgid "Open File..."
#~ msgstr "افتح ملف..."
